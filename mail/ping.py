import socket
import time

interval = 5

port = 3004
ip = '192.168.100.255'


def pinging(name):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    while True:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.sendto(name.encode('utf8'), (ip, port))

        time.sleep(interval)


def recvPing(map):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('', port))
    while True:
        name, ip = s.recvfrom(4096)
        name = name.decode('utf-8')

        map.update({name: ip[0]})
