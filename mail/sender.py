import socket
from threading import Thread
import ping

port = 3003

name = input('Please enter your name:')
while name.find(':') != -1:
    print('Names cannot contain ":"!')
    name = input('Please enter your name:')

#t = Thread(target=ping.pinging, args=(name,))
#t.start()

d = {}
t = Thread(target=ping.recvPing, args=(d,))
t.start()

while True:
    dest_name = input('Where should the message go?')
    if dest_name in d:
        dest_address = d[dest_name]
    else:
        dest_address = dest_name
    text = input('Please enter your message:')


    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((dest_address, port))
    message = '%s:%s' % (name, text)
    message_bytes = message.encode('utf8')
    s.send(message_bytes)
    s.close()
