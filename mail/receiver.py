import socket
from threading import Thread
import ping

port = 3003

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', port))
s.listen()

name = input('Please enter your name:')
while name.find(':') != -1:
    print('Names cannot contain ":"!')
    name = input('Please enter your name:')

t = Thread(target=ping.pinging, args=(name,))
t.start()

while True:
    conn, address = s.accept()

    message = ""
    while True:
        new = conn.recv(1024).decode('utf8')
        if len(new) == 0:
            break
        message += new

    try:
        name, text = message.split(':', 1)
    except ValueError:
        conn.send('Unknown Protocol'.encode('utf8'))
        conn.close()
        continue
    conn.close()
    print('[%s]: %s' % (name, text))
